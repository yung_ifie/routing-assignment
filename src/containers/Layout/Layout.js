import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Layout.css';

export class Layout extends Component {
  render() {
    return (
      <div className="Layout">
        <header>
          <nav>
            <ul>
              <li>
                <NavLink to="/users" exact>Users</NavLink>
              </li>
              <li>
                <NavLink to="/courses">Courses</NavLink>
              </li>
            </ul>
          </nav>
        </header>
        {this.props.children}
      </div>
    );
  }
}

export default Layout;
